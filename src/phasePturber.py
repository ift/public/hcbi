# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift


class phasePtb(ift.LinearOperator):
    def __init__(
        self,
        ph_screen_key,
        ph_screen_dom,
        y_shift_key,
        y_shift_dom,
        x_shift_key,
        x_shift_dom,
    ):
        self._domain = ift.MultiDomain.make(
            {
                ph_screen_key: ph_screen_dom,
                y_shift_key: y_shift_dom,
                x_shift_key: x_shift_dom,
            }
        )
        self._target = ift.makeDomain(ph_screen_dom)

        self._ph_screen_dom = ph_screen_dom
        self._ph_screen_key = ph_screen_key

        self._y_shift_dom = y_shift_dom
        self._y_shift_key = y_shift_key

        self._x_shift_dom = x_shift_dom
        self._x_shift_key = x_shift_key

        self._x_slope = np.linspace(0, 1, ph_screen_dom.shape[1])
        self._y_slope = np.linspace(0, 1, ph_screen_dom.shape[2])

        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            ph_screen = x[self._ph_screen_key]
            y_shift = x[self._y_shift_key]
            x_shift = x[self._x_shift_key]

            x_shift_ph = np.einsum("i,j -> ij", x_shift.val, self._x_slope)
            x_shift_ph = np.expand_dims(x_shift_ph, axis=1)

            y_shift_ph = np.einsum("i,j -> ij", y_shift.val, self._y_slope)
            y_shift_ph = np.expand_dims(y_shift_ph, axis=2)

            res = np.add(ph_screen.val, x_shift_ph)
            res = np.add(res, y_shift_ph)
            res = ift.Field.from_raw(self._target, res)
            return res
        else:
            sum1 = np.sum(x.val, axis=1)
            sum2 = np.sum(x.val, axis=2)

            x_shift = np.einsum("ij,j -> i", sum1, self._x_slope)
            y_shift = np.einsum("ij,j -> i", sum2, self._y_slope)

            res = {
                self._ph_screen_key: x.val,
                self._x_shift_key: x_shift,
                self._y_shift_key: y_shift,
            }
            res = ift.MultiField.from_raw(self._domain, res)
            return res
