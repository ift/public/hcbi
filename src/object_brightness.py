# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np
import ast

from functools import reduce

from src.star import star
from src.domChanger import DomainChanger


class ObjectBrightness:
    def __init__(self, configpars):
        def lognormal_moment_matching(mean, sig, key):
            mean, sig = np.asfarray(mean), np.asfarray(sig)
            logsig = np.sqrt(np.log((sig / mean) ** 2 + 1))
            logmean = np.log(mean) - logsig ** 2 / 2

            dom = ift.DomainTuple.scalar_domain()
            add = ift.Adder(ift.Field.from_raw(dom, logmean))
            scl = ift.DiagonalOperator(ift.Field.from_raw(dom, logsig)).ducktape(key)
            return (add @ scl).exp()

        def normal_dist(dom, key, mean=None, var=None):
            if mean is None:
                add = ift.ScalingOperator(dom, 1.0)
            else:
                add = ift.Adder(mean)
            if var is None:
                scl = ift.DiagonalOperator(ift.full(dom, 1.0)).ducktape(key)
            else:
                scl = ift.DiagonalOperator(var).ducktape(key)
            return add @ scl

        def get_centerd_circle(center, fraction, dom):
            dist = dom.distances
            shp = dom.shape
            ds = dist[0] * shp[0]
            pts = np.mgrid[0 : ds : dist[0], 0 : ds : dist[1]]
            pts = np.dstack(pts)
            xCtr = center[0] * ds
            yCtr = center[1] * ds

            ctr = np.dstack([xCtr, yCtr])

            rad_vec = pts - ctr
            # rad_vec = pts
            rad_vec[rad_vec[:, :, :] > 0.5 * ds] = (
                rad_vec[rad_vec[:, :, :] > 0.5 * ds] - 1 * ds
            )
            rad_vec[rad_vec[:, :, :] < -0.5 * ds] = (
                rad_vec[rad_vec[:, :, :] < -0.5 * ds] + 1 * ds
            )
            rad = np.linalg.norm(rad_vec, axis=2)
            res = np.zeros(shp)
            radius = fraction * ds
            res[rad < radius] = 1
            return res

        brightness_conf = dict(configpars.items("brightness_args"))
        activate_star = int(brightness_conf["activate_star"])
        planets = int(brightness_conf["planets"])
        lognormal_planets = int(brightness_conf["lognormal_planets"])
        back = int(brightness_conf["back"])
        correl_back = int(brightness_conf["correl_back"])
        planets_block_fraction = float(brightness_conf["mask_fraction"])

        pixel_conf = dict(configpars.items("pixel_args"))
        sensor_pix_x = int(pixel_conf["sensor_pix_x"])
        sensor_pix_y = int(pixel_conf["sensor_pix_y"])
        sensor_pix_dist = float(pixel_conf["sensor_pix_dist"])
        sensors_mask = float(pixel_conf["sensor_mask"])

        pos_pix_x = int(sensor_pix_x + sensors_mask * sensor_pix_x)
        pos_pix_y = int(sensor_pix_y + sensors_mask * sensor_pix_y)

        harmonic_space = ift.RGSpace(
            [pos_pix_x, pos_pix_y], distances=sensor_pix_dist, harmonic=True
        )
        position_space = harmonic_space.get_default_codomain()
        add_int_ops = []

        FFT = ift.FFTOperator(harmonic_space)
        self.FFT = ift.FFTOperator(harmonic_space)

        if planets == 1:

            if planets_block_fraction > 0:
                star_block = get_centerd_circle(
                    [0.5, 0.5], planets_block_fraction, harmonic_space
                )
                star_block = ift.Field.from_raw(harmonic_space, star_block)
                star_block = ift.MaskOperator(star_block)
            else:
                star_block = ift.ScalingOperator(harmonic_space, 1.0)
            # create inverse gamma field wave
            if lognormal_planets == 1:
                lognormal_planets_params = ast.literal_eval(
                    brightness_conf["pts_lognormal"]
                )
                mean = lognormal_planets_params["mean"] / position_space.dvol
                sig = lognormal_planets_params["sig"] / position_space.dvol
                key = lognormal_planets_params["key"]
                logsig = np.sqrt(np.log((sig / mean) ** 2 + 1))
                logmean = np.log(mean) - logsig ** 2 / 2

                dom = ift.DomainTuple.scalar_domain()
                add = ift.Adder(ift.full(star_block.target, logmean))
                scl = ift.ScalingOperator(star_block.target, logsig).ducktape(key)
                pts_pos_space = (add @ scl).exp()
            else:
                self.unit_gaus = normal_dist(star_block.target, "gaus_intensity")
                pts_pos_space = (
                    ift.InverseGammaOperator(
                        self.unit_gaus.target,
                        **ast.literal_eval(brightness_conf["pts"])
                    )
                    @ self.unit_gaus
                )
            # create dark ring around star
            pts_intensity = star_block.adjoint @ pts_pos_space

            rlz_pts = ift.Realizer(pts_intensity.target)
            self.pts_waves = (
                FFT @ rlz_pts.adjoint @ pts_intensity.clip(a_min=0, a_max=1e4)
            )
            add_int_ops.append(self.pts_waves)

        if activate_star == 1:
            # create star point source wave
            self.st_intensity = lognormal_moment_matching(
                **ast.literal_eval(brightness_conf["star"])
            )
            self.star = star(position_space, 0.5, 0.5) @ self.st_intensity

            add_int_ops.append(self.star)

        if correl_back == 1:
            correl_back_conf = dict(configpars.items("correl_back_conf"))
            cfmaker_correl_back = ift.CorrelatedFieldMaker(
                **ast.literal_eval(correl_back_conf["cfmaker_make"])
            )
            cfmaker_correl_back.add_fluctuations(
                position_space, **ast.literal_eval(correl_back_conf["cfmaker_fl"])
            )
            cfmaker_correl_back.set_amplitude_total_offset(
                    **ast.literal_eval(correl_back_conf["cfmaker_offset"])
            )
            correl_back_op_rl = cfmaker_correl_back.finalize(
                **ast.literal_eval(correl_back_conf["cfmaker_final"])
            )
            realizer_c_b = ift.Realizer(correl_back_op_rl.target)
            self.correl_back_op = realizer_c_b.adjoint @ ift.exp(correl_back_op_rl)
            dom_ch = DomainChanger(self.correl_back_op.target, (harmonic_space,))
            self.correl_back_wave = FFT @ dom_ch @ self.correl_back_op
            add_int_ops.append(self.correl_back_wave)

        if back:
            self.unit_gaus_back = normal_dist(
                ift.DomainTuple.scalar_domain(), "gaus_intensity_back"
            )
            inv_gamma_back = ift.InverseGammaOperator(
                self.unit_gaus_back.target,
                **ast.literal_eval(brightness_conf["back_params"])
            )

            intensity_back = inv_gamma_back @ self.unit_gaus_back
            unit_field = ift.full(harmonic_space, 1.0)
            intensity_back_field = ift.VdotOperator(unit_field).adjoint @ intensity_back
            rlz_back = ift.Realizer(intensity_back_field.target)
            self.intensity_back_wave = (
                FFT @ rlz_back.adjoint @ intensity_back_field.clip(a_min=0, a_max=1e4)
            )
            add_int_ops.append(self.intensity_back_wave)

        if len(add_int_ops) == 0:
            raise RuntimeError("No object intensity component")

        self.object_intensity = reduce(lambda i1, i2: i1 + i2, add_int_ops)
