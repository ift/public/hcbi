# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


def unit_point_source_harmonic_space(pos_x, pos_y, position_space):
    harmonic_space = position_space.get_default_codomain()
    FFT = ift.FFTOperator(harmonic_space)

    unit_star = np.zeros(position_space.shape)
    unit_star[int(pos_x * unit_star.shape[0]), int(pos_y * unit_star.shape[1])] = (
        1.0e-5 / harmonic_space.dvol
    )
    unit_star = ift.Field.from_raw(position_space, unit_star)
    unit_wave = FFT.inverse(unit_star)
    return unit_wave
