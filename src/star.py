# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift


class star(ift.LinearOperator):
    def __init__(self, target, pos0, pos1):
        self._domain = ift.DomainTuple.make(())
        self._target = ift.makeDomain(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES

        unit_star = np.zeros(target.shape)
        unit_star[int(target.shape[0] * pos0), int(target.shape[0] * pos1)] = (
            1.0 / target.dvol
        )

        pos_sp = target.get_default_codomain()
        unit_star = ift.Field.from_raw(pos_sp, unit_star)

        FFT = ift.FFTOperator(target)
        self._unit_wave = FFT.inverse(unit_star)
        self._vdot = ift.VdotOperator(self._unit_wave)
        self._rlz = ift.Realizer(self._vdot.target)

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return x.val * self._unit_wave
        else:
            return self._rlz(self._vdot(x))
