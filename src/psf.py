# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import ast

from src.aperture import aperture
from src.helper import unit_point_source_harmonic_space
from src.phasePturber import phasePtb
from src.GeometricMask import GeomMaskOperator
from src.wavePturber import wavePtb
from src.absValsq import absVal


class PSF:
    def __init__(self, configpars, weight_psf_two=1):

        data_conf = dict(configpars.items("data_args"))
        wavelength = float(data_conf["wavelength"])

        # pixel configuration
        pixel_conf = dict(configpars.items("pixel_args"))
        sensor_pix_x = int(pixel_conf["sensor_pix_x"])
        sensor_pix_y = int(pixel_conf["sensor_pix_y"])
        sensor_pix_dist = float(pixel_conf["sensor_pix_dist"])
        sensors_mask = float(pixel_conf["sensor_mask"])
        time_pix = int(pixel_conf["time_pix"])
        time_pix_dist = float(pixel_conf["time_pix_dist"])
        time_mask = float(pixel_conf["time_mask"])

        pos_pix_x = int(sensor_pix_x + sensors_mask * sensor_pix_x)
        pos_pix_y = int(sensor_pix_y + sensors_mask * sensor_pix_y)

        harmonic_space = ift.RGSpace(
            [pos_pix_x, pos_pix_y], distances=sensor_pix_dist, harmonic=True
        )
        harmonic_space_obs = ift.RGSpace(
            [sensor_pix_x, sensor_pix_y], distances=sensor_pix_dist, harmonic=True
        )
        position_space = harmonic_space.get_default_codomain()

        time_space = ift.RGSpace(time_pix + time_mask, distances=time_pix_dist)
        time_space_obs = ift.RGSpace(time_pix, distances=time_pix_dist)

        obs_sp = ift.DomainTuple.make((time_space_obs, harmonic_space_obs))

        model_conf = dict(configpars.items("model_args"))
        path_length_model = model_conf["path_length"]
        gradient_fields = int(model_conf["gradient_fields"])

        flux_conf = dict(configpars.items("flux_scale"))
        cfmaker_fluxScale = ift.CorrelatedFieldMaker(
            **ast.literal_eval(flux_conf["cfmaker_make"])
        )
        cfmaker_fluxScale.add_fluctuations(
            time_space, **ast.literal_eval(flux_conf["cfmaker_fl_time"])
        )
        cfmaker_fluxScale.set_amplitude_total_offset(
                **ast.literal_eval(flux_conf["cfmaker_offset"])
        )
        flux_scale_rl = cfmaker_fluxScale.finalize(
            **ast.literal_eval(flux_conf["cfmaker_final"])
        )
        realizer_f_s = ift.Realizer(flux_scale_rl.target)
        self.fluxScale = realizer_f_s.adjoint @ flux_scale_rl

        shift_x_conf = dict(configpars.items("shiftX"))
        cfmaker_shift = ift.CorrelatedFieldMaker(
            **ast.literal_eval(shift_x_conf["cfmaker_make"])
        )
        cfmaker_shift.add_fluctuations(
            time_space, **ast.literal_eval(shift_x_conf["cfmaker_fl_time"])
        )
        cfmaker_shift.set_amplitude_total_offset(
                **ast.literal_eval(shift_x_conf["cfmaker_offset"])
        )
        self.x_shift = cfmaker_shift.finalize(
            **ast.literal_eval(shift_x_conf["cfmaker_final"])
        )

        shift_x2_conf = dict(configpars.items("shiftX_two"))
        cfmaker_shift2 = ift.CorrelatedFieldMaker(
            **ast.literal_eval(shift_x2_conf["cfmaker_make"])
        )
        cfmaker_shift2.add_fluctuations(
            time_space, **ast.literal_eval(shift_x2_conf["cfmaker_fl_time"])
        )
        cfmaker_shift2.set_amplitude_total_offset(
                **ast.literal_eval(shift_x2_conf["cfmaker_offset"])
        )
        self.x_shift2 = cfmaker_shift2.finalize(
            **ast.literal_eval(shift_x2_conf["cfmaker_final"])
        )

        shift_y_conf = dict(configpars.items("shiftY"))
        cfmaker_shifty = ift.CorrelatedFieldMaker(
            **ast.literal_eval(shift_y_conf["cfmaker_make"])
        )
        cfmaker_shifty.add_fluctuations(
            time_space, **ast.literal_eval(shift_y_conf["cfmaker_fl_time"])
        )
        cfmaker_shifty.set_amplitude_total_offset(
                **ast.literal_eval(shift_y_conf["cfmaker_offset"])
        )
        self.y_shift = cfmaker_shifty.finalize(
            **ast.literal_eval(shift_y_conf["cfmaker_final"])
        )

        shift_y2_conf = dict(configpars.items("shiftY_two"))
        cfmaker_shifty2 = ift.CorrelatedFieldMaker(
            **ast.literal_eval(shift_y2_conf["cfmaker_make"])
        )
        cfmaker_shifty2.add_fluctuations(
            time_space, **ast.literal_eval(shift_y2_conf["cfmaker_fl_time"])
        )
        cfmaker_shifty2.set_amplitude_total_offset(
                **ast.literal_eval(shift_y2_conf["cfmaker_offset"])
        )
        self.y_shift2 = cfmaker_shifty2.finalize(
            **ast.literal_eval(shift_y2_conf["cfmaker_final"])
        )

        self.aperture = aperture(
            position_space,
            n_pixel=sensor_pix_x,
            mas_per_pixel=sensor_pix_dist,
            wavelength_m=wavelength,
        )

        self.FFT_image = ift.FFTOperator(
            ift.DomainTuple.make((time_space, harmonic_space)), space=1
        )
        self.HT_image = ift.HartleyOperator(
            ift.DomainTuple.make((time_space, harmonic_space)), space=1
        )

        plain_wave = unit_point_source_harmonic_space(0.0, 0.0, harmonic_space)
        self.plain_wave_aperture = ift.DiagonalOperator(plain_wave)(self.aperture)

        if path_length_model == "single":
            from src.path_length import PathLength

            self.pl = PathLength(configpars)
            self.phase_screen = self.pl.phase_screen
        elif path_length_model == "double":
            from src.path_length_double import PathLength

            self.pl = PathLength(configpars)
            self.phase_screen_one = self.pl.phase_screen_one
            self.phase_screen_two = self.pl.phase_screen_two
        else:
            raise RuntimeError("incorrect phase screen config")

        if path_length_model == "double":
            if gradient_fields == 1:
                mf_phase_screen = ift.FieldAdapter(
                    self.phase_screen_one.target, "ph_scr"
                )
                mf_x_shift = ift.FieldAdapter(self.x_shift.target, "x_sh")
                mf_y_shift = ift.FieldAdapter(self.y_shift.target, "y_sh")
                mf_phases = (
                    mf_phase_screen.adjoint(self.phase_screen_one)
                    + mf_x_shift.adjoint(self.x_shift)
                    + mf_y_shift.adjoint(self.y_shift)
                )

                phase_ptb = phasePtb(
                    "ph_scr",
                    self.phase_screen_one.target,
                    "y_sh",
                    self.y_shift.target,
                    "x_sh",
                    self.x_shift.target,
                )
                self.total_phase_one = phase_ptb(mf_phases)
                rlz_phase = ift.Realizer(self.total_phase_one.target)
                ph_shift_one = ift.exp(1j * rlz_phase.adjoint(self.total_phase_one))

                mf_phase_screen2 = ift.FieldAdapter(
                    self.phase_screen_two.target, "ph_scr2"
                )
                mf_x_shift2 = ift.FieldAdapter(self.x_shift2.target, "x_sh2")
                mf_y_shift2 = ift.FieldAdapter(self.y_shift2.target, "y_sh2")
                mf_phases2 = (
                    mf_phase_screen2.adjoint(self.phase_screen_two)
                    + mf_x_shift2.adjoint(self.x_shift2)
                    + mf_y_shift2.adjoint(self.y_shift2)
                )

                phase_ptb2 = phasePtb(
                    "ph_scr2",
                    self.phase_screen_two.target,
                    "y_sh2",
                    self.y_shift.target,
                    "x_sh2",
                    self.x_shift.target,
                )
                self.total_phase_two = phase_ptb2(mf_phases2)
                rlz_phase2 = ift.Realizer(self.total_phase_two.target)
                ph_shift_two = ift.exp(1j * rlz_phase2.adjoint(self.total_phase_two))
            else:
                rlz_phase_one = ift.Realizer(self.phase_screen_one.target)
                rlz_phase_two = ift.Realizer(self.phase_screen_two.target)
                ph_shift_one = ift.exp(
                    1j * rlz_phase_one.adjoint(self.phase_screen_one)
                )
                ph_shift_two = ift.exp(
                    1j * rlz_phase_two.adjoint(self.phase_screen_two)
                )

            mf_ptb_one = ift.FieldAdapter(ph_shift_one.target, "ptb_one")
            mf_scal_one = ift.FieldAdapter(self.fluxScale.target, "scl_one")
            mf_ptb_two = ift.FieldAdapter(ph_shift_two.target, "ptb_two")
            mf_scal_two = ift.FieldAdapter(self.fluxScale.target, "scl_two")

            mf_apt_one = mf_ptb_one.adjoint(ph_shift_one) + mf_scal_one.adjoint(
                self.fluxScale
            )
            mf_apt_two = mf_ptb_two.adjoint(ph_shift_two) + mf_scal_two.adjoint(
                self.fluxScale
            )
            w_ptb_one = wavePtb(
                self.plain_wave_aperture,
                ph_shift_one.target,
                "ptb_one",
                self.fluxScale.target,
                "scl_one",
            )
            w_ptb_two = wavePtb(
                self.plain_wave_aperture,
                ph_shift_two.target,
                "ptb_two",
                self.fluxScale.target,
                "scl_two",
            )

            wave_begind_apt_one = w_ptb_one @ mf_apt_one
            wave_begind_apt_two = w_ptb_two @ mf_apt_two

            complex_psf_one = self.FFT_image.inverse(wave_begind_apt_one)
            complex_psf_two = self.FFT_image.inverse(wave_begind_apt_two)
            sensors_mask_one = GeomMaskOperator(complex_psf_one.target, obs_sp)
            sensors_mas_two = GeomMaskOperator(complex_psf_two.target, obs_sp)

            absq_one = absVal(complex_psf_one.target)
            absq_two = absVal(complex_psf_two.target)
            self.psf_one = absq_one @ complex_psf_one
            self.psf_two = absq_two @ complex_psf_two
            if weight_psf_two == 0.0:
                self.psf = self.psf_one
            else:
                self.psf = self.psf_one + weight_psf_two * self.psf_two
        else:
            if gradient_fields == 1:
                mf_phase_screen = ift.FieldAdapter(self.phase_screen.target, "ph_scr")
                mf_x_shift = ift.FieldAdapter(self.x_shift.target, "x_sh")
                mf_y_shift = ift.FieldAdapter(self.y_shift.target, "y_sh")
                mf_phases = (
                    mf_phase_screen.adjoint(self.phase_screen)
                    + mf_x_shift.adjoint(self.x_shift)
                    + mf_y_shift.adjoint(self.y_shift)
                )

                phase_ptb = phasePtb(
                    "ph_scr",
                    self.phase_screen.target,
                    "y_sh",
                    self.y_shift.target,
                    "x_sh",
                    self.x_shift.target,
                )
                self.total_phase = phase_ptb(mf_phases)
                rlz_phase = ift.Realizer(self.total_phase.target)
                ph_shift = ift.exp(1j * rlz_phase.adjoint(self.total_phase))
            else:
                rlz_phase = ift.Realizer(self.phase_screen.target)
                ph_shift = ift.exp(1j * rlz_phase.adjoint(self.phase_screen))

            mf_ptb = ift.FieldAdapter(ph_shift.target, "ptb")
            mf_scal = ift.FieldAdapter(self.fluxScale.target, "scl")

            mf_apt = mf_ptb.adjoint(ph_shift) + mf_scal.adjoint(self.fluxScale)
            w_ptb = wavePtb(
                self.plain_wave_aperture,
                ph_shift.target,
                "ptb",
                self.fluxScale.target,
                "scl",
            )

            wave_begind_apt = w_ptb @ mf_apt

            complex_psf = self.FFT_image.inverse(wave_begind_apt)
            sensors_mask = GeomMaskOperator(complex_psf.target, obs_sp)

            conjOP = ift.ConjugationOperator(complex_psf.target)
            rls_psf = ift.Realizer(complex_psf.target)
            absq = absVal(complex_psf.target)
            self.psf = absq @ complex_psf
