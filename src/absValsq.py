# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift


class absVal(ift.Operator):
    def __init__(self, domain):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(domain)

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val
        res = xval.absolute() ** 2
        if not lin:
            return res
        rlz = ift.Realizer(self._domain)
        igz = Imagizer(self._domain)
        dr = ift.DiagonalOperator(2 * xval.real)
        di = ift.DiagonalOperator(2 * xval.imag)
        return x.new(res, ((dr @ rlz) + (di @ igz)))


class Imagizer(ift.EndomorphicOperator):
    """Operator returning the imaginary component of its input.

    Parameters
    ----------
    domain: Domain, tuple of domains or DomainTuple
        domain of the input field

    """

    def __init__(self, domain):
        self._domain = ift.DomainTuple.make(domain)
        self._Rl = ift.Realizer(domain)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return self._Rl(-1j * x)
        if mode == self.ADJOINT_TIMES:
            return 1j * x
        """
        try:
            return x.imag
        except:
            return ift.full(self._domain, 0.)
        """
        res = -1j * x
        return self._Rl(res)
