# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift


def get_circle(center, n_pix, radius, inside, outide):
    pts = np.mgrid[0:n_pix:1, 0:n_pix:1]
    pts = np.dstack(pts)
    ctr = np.dstack([center[0], center[1]])
    rad_vec = pts - ctr
    rad = np.linalg.norm(rad_vec, axis=2)
    res = np.ones((n_pix, n_pix))
    res[rad < radius] = inside
    res[rad >= radius] = outide
    return res


def aperture(
    space,
    size_main_mirror=8.2,
    size_secondary_mirror=0.95,
    n_pixel=100,
    mas_per_pixel=12.844,
    wavelength_m=630e-9,
):
    if not space.distances[0] == space.distances[1]:
        raise NameError("distances in x and y direction are different")

    def mas_to_rad(a):
        return a * 1e-3 * ((2 * np.pi) / (360 * 60 ** 2))

    pixel_dist = wavelength_m / (n_pixel * mas_to_rad(mas_per_pixel))
    number_pixels_main_mirror = size_main_mirror / pixel_dist
    number_pixels_secondary_mirror = size_secondary_mirror / pixel_dist
    center = [space.shape[0] / 2, space.shape[1] / 2]
    primary = get_circle(center, space.shape[0], number_pixels_main_mirror / 2.0, 1, 0)
    secondary = get_circle(
        center, space.shape[0], number_pixels_secondary_mirror / 2.0, 0, 1
    )
    apt = primary * secondary
    return ift.Field.from_raw(space, apt)
