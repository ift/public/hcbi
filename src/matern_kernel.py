# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift

from functools import reduce
from operator import mul


class _AmplitudeMatern(ift.Operator):
    def __init__(self, pow_spc, scale, cutoff, logloghalfslope, azm, totvol):
        expander = ift.ContractionOperator(pow_spc, spaces=None).adjoint
        k_squared = ift.makeField(pow_spc, pow_spc.k_lengths ** 2)

        a = expander @ scale.log()
        b = ift.VdotOperator(k_squared).adjoint @ cutoff.power(-2.0)
        c = expander.scale(-1) @ logloghalfslope

        ker = ift.Adder(ift.full(pow_spc, 1.0)) @ b
        ker = c * ker.log() + a
        op = ker.exp()

        # Account for the volume of the position space (dvol in harmonic space
        # is 1/volume-of-position-space) in the definition of the amplitude as
        # to make the parametric model agnostic to changes in the volume of the
        # position space
        vol0, vol1 = [np.zeros(pow_spc.shape) for _ in range(2)]
        # The zero-mode scales linearly with the volume in position space
        vol0[0] = totvol
        # Variances decrease linearly with the volume in position space after a
        # harmonic transformation (var{HT(randn)} \propto 1/\sqrt{totvol} for
        # randn standard normally distributed variables).  This needs to be
        # accounted for in the amplitude model.
        vol1[1:] = totvol ** 0.5
        vol0 = ift.Adder(ift.makeField(pow_spc, vol0))
        vol1 = ift.DiagonalOperator(ift.makeField(pow_spc, vol1))
        op = vol0 @ (vol1 @ (expander @ azm.ptw("reciprocal")) * op)

        # std = sqrt of integral of power spectrum
        self._fluc = op.power(2).integrate().sqrt()
        self.apply = op.apply
        self._domain, self._target = op.domain, op.target
        self._repr_str = "_AmplitudeMatern: " + op.__repr__()

    @property
    def fluctuation_amplitude(self):
        return self._fluc

    def __repr__(self):
        return self._repr_str


class CorrelatedFieldMaker:
    """Construction helper for hierarchical correlated field models.

    The correlated field models are parametrized by creating
    square roots of power spectrum operators ("amplitudes") via calls to
    :func:`add_fluctuations` that act on the targeted field subdomains.
    During creation of the :class:`CorrelatedFieldMaker` via
    :func:`make`, a global offset from zero of the field model
    can be defined and an operator applying fluctuations
    around this offset is parametrized.

    The resulting correlated field model operator has a
    :class:`~nifty7.multi_domain.MultiDomain` as its domain and
    expects its input values to be univariately gaussian.

    The target of the constructed operator will be a
    :class:`~nifty7.domain_tuple.DomainTuple` containing the
    `target_subdomains` of the added fluctuations in the order of
    the `add_fluctuations` calls.

    Creation of the model operator is completed by calling the method
    :func:`finalize`, which returns the configured operator.

    An operator representing an array of correlated field models
    can be constructed by setting the `total_N` parameter of
    :func:`make`. It will have an
    :class:`~nifty7.domains.unstructured_domain.UnstructuredDomain`
    of shape `(total_N,)` prepended to its target domain and represent
    `total_N` correlated fields simulataneously.
    The degree of information sharing between the correlated field
    models can be configured via the `dofdex` parameters
    of :func:`make` and :func:`add_fluctuations`.

    See the methods :func:`make`, :func:`add_fluctuations`
    and :func:`finalize` for further usage information."""

    def __init__(self, offset_mean, offset_fluctuations_op, prefix, total_N):
        if not isinstance(offset_fluctuations_op, ift.Operator):
            raise TypeError("offset_fluctuations_op needs to be an operator")
        self._a = []
        self._target_subdomains = []

        self._offset_mean = offset_mean
        self._azm = offset_fluctuations_op
        self._prefix = prefix
        self._total_N = total_N

    @staticmethod
    def make(offset_mean, offset_std, prefix, total_N=0, dofdex=None):
        """Returns a CorrelatedFieldMaker object.

        Parameters
        ----------
        offset_mean : float
            Mean offset from zero of the correlated field to be made.
        offset_std : tuple of float
            Mean standard deviation and standard deviation of the standard
            deviation of the offset. No, this is not a word duplication.
        prefix : string
            Prefix to the names of the domains of the cf operator to be made.
            This determines the names of the operator domain.
        total_N : integer, optional
            Number of field models to create.
            If not 0, the first entry of the operators target will be an
            :class:`~nifty.domains.unstructured_domain.UnstructuredDomain`
            with length `total_N`.
        dofdex : np.array of integers, optional
            An integer array specifying the zero mode models used if
            total_N > 1. It needs to have length of total_N. If total_N=3 and
            dofdex=[0,0,1], that means that two models for the zero mode are
            instantiated; the first one is used for the first and second
            field model and the second is used for the third field model.
            *If not specified*, use the same zero mode model for all
            constructed field models.
        """
        if dofdex is None:
            dofdex = np.full(total_N, 0)
        elif len(dofdex) != total_N:
            raise ValueError("length of dofdex needs to match total_N")
        N = max(dofdex) + 1 if total_N > 0 else 0
        if len(offset_std) != 2:
            raise TypeError
        zm = ift.LognormalTransform(*offset_std, prefix + "zeromode", N)
        if total_N > 0:
            zm = _Distributor(dofdex, zm.target, UnstructuredDomain(total_N)) @ zm
        return CorrelatedFieldMaker(offset_mean, zm, prefix, total_N)

    def add_fluctuations_matern(
        self,
        target_subdomain,
        scale,
        cutoff,
        logloghalfslope,
        prefix="",
        adjust_for_volume=True,
        harmonic_partner=None,
    ):
        """Function to add matern kernels to the field to be made.

        The matern kernel amplitude is parametrized in the following way:

        .. math ::
            A(|k|) = \\frac{a}{\\left(1 + { \
                \\left(\\frac{|k|}{b}\\right) \
            }^2\\right)^c}

        where :math:`a` is the scale, :math:`b` the cutoff and :math:`c` half
        the slope of the power law.

        Parameters
        ----------
        target_subdomain : :class:`~nifty7.domains.domain.Domain`, \
                           :class:`~nifty7.domain_tuple.DomainTuple`
            Target subdomain on which the correlation structure defined
            in this call should hold.
        scale : tuple of float (mean, std)
            Overall scale of the fluctuations in the target subdomain.
        cutoff : tuple of float (mean, std)
            Frequency at which the power spectrum should transition into
            a spectra following a power-law.
        logloghalfslope : tuple of float (mean, std)
            Half of the slope of the amplitude spectrum.
        prefix : string
            Prefix of the power spectrum parameter domain names.
        adjust_for_volume : bool, optional
            Whether to implicitly adjust the scale parameter of the Matern
            kernel and the zero-mode of the overall model for the volume in the
            target subdomain or assume them to be adjusted already.
        harmonic_partner : :class:`~nifty7.domains.domain.Domain`, \
                           :class:`~nifty7.domain_tuple.DomainTuple`
            Harmonic space in which to define the power spectrum.

        Notes
        -----
        The parameters of the amplitude model are assumed to be relative to a
        unit-less power spectrum, i.e. the parameters are assumed to be
        agnostic to changes in the volume of the target subdomain. This is in
        steep contrast to the non-parametric amplitude operator in
        :class:`~nifty7.library.correlated_fields.CorrelatedFieldMaker.add_fluctuations`.

        Up to the Matern amplitude only works for `total_N == 0`.
        """
        if self._total_N > 0:
            raise NotImplementedError()
        if harmonic_partner is None:
            harmonic_partner = target_subdomain.get_default_codomain()
        else:
            target_subdomain.check_codomain(harmonic_partner)
            harmonic_partner.check_codomain(target_subdomain)
        target_subdomain = ift.makeDomain(target_subdomain)

        scale = ift.LognormalTransform(*scale, self._prefix + prefix + "scale", 0)
        prfx = self._prefix + prefix + "cutoff"
        cutoff = ift.LognormalTransform(*cutoff, prfx, 0)
        prfx = self._prefix + prefix + "logloghalfslope"
        logloghalfslope = ift.NormalTransform(*logloghalfslope, prfx, 0)

        totvol = 1.0
        if adjust_for_volume:
            totvol = target_subdomain[-1].total_volume
        pow_spc = ift.PowerSpace(harmonic_partner)
        amp = _AmplitudeMatern(
            pow_spc, scale, cutoff, logloghalfslope, self._azm, totvol
        )

        self._a.append(amp)
        self._target_subdomains.append(target_subdomain)

    def finalize(self):
        """Finishes model construction process and returns the constructed
        operator.

        Parameters
        ----------
        prior_info : integer
            How many prior samples to draw for property verification statistics
            If zero, skips calculating and displaying statistics.
        """
        n_amplitudes = len(self._a)
        if self._total_N > 0:
            hspace = ift.makeDomain(
                [ift.UnstructuredDomain(self._total_N)]
                + [dd.target[-1].harmonic_partner for dd in self._a]
            )
            spaces = tuple(range(1, n_amplitudes + 1))
            amp_space = 1
        else:
            hspace = ift.makeDomain([dd.target[0].harmonic_partner for dd in self._a])
            spaces = tuple(range(n_amplitudes))
            amp_space = 0

        expander = ift.ContractionOperator(hspace, spaces=spaces).adjoint
        azm = expander @ self._azm

        ht = ift.HarmonicTransformOperator(
            hspace, self._target_subdomains[0][amp_space], space=spaces[0]
        )
        for i in range(1, n_amplitudes):
            ht = (
                ift.HarmonicTransformOperator(
                    ht.target, self._target_subdomains[i][amp_space], space=spaces[i]
                )
                @ ht
            )
        a = []
        for ii in range(n_amplitudes):
            co = ift.ContractionOperator(hspace, spaces[:ii] + spaces[ii + 1 :])
            pp = self._a[ii].target[amp_space]
            pd = ift.PowerDistributor(co.target, pp, amp_space)
            a.append(co.adjoint @ pd @ self._a[ii])
        corr = reduce(mul, a)
        op = ht(azm * corr * ift.ducktape(hspace, None, self._prefix + "xi"))

        if self._offset_mean is not None:
            offset = self._offset_mean
            # Deviations from this offset must not be considered here as they
            # are learned by the zeromode
            if isinstance(offset, (ift.Field, ift.MultiField)):
                op = ift.Adder(offset) @ op
            else:
                offset = float(offset)
                op = ift.Adder(ift.full(op.target, offset)) @ op
        return op

    def moment_slice_to_average(self, fluctuations_slice_mean, nsamples=1000):
        fluctuations_slice_mean = float(fluctuations_slice_mean)
        if not fluctuations_slice_mean > 0:
            msg = "fluctuations_slice_mean must be greater zero; got {!r}"
            raise ValueError(msg.format(fluctuations_slice_mean))
        from ..sugar import from_random

        scm = 1.0
        for a in self._a:
            op = a.fluctuation_amplitude * self._azm.ptw("reciprocal")
            res = np.array(
                [op(from_random(op.domain, "normal")).val for _ in range(nsamples)]
            )
            scm *= res ** 2 + 1.0
        return fluctuations_slice_mean / np.mean(np.sqrt(scm))

    @property
    def normalized_amplitudes(self):
        """Returns the amplitude operators used in the model"""
        return self._a

    @property
    def amplitude(self):
        if len(self._a) > 1:
            s = (
                "If more than one spectrum is present in the model,",
                " no unique set of amplitudes exist because only the",
                " relative scale is determined.",
            )
            raise NotImplementedError(s)
        dom = self._a[0].target
        expand = ift.ContractionOperator(dom, len(dom) - 1).adjoint
        return self._a[0] * (expand @ self.amplitude_total_offset)

    @property
    def power_spectrum(self):
        return self.amplitude ** 2

    @property
    def amplitude_total_offset(self):
        return self._azm
