# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np
import os

from astropy.io import fits
from src.high_contrast_imaging import HighContrasImaging

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    n_task = comm.Get_size()
    rank = comm.Get_rank()
except ImportError:

    comm = None
    n_task = 1
    rank = 0

master = rank == 0


def dump_data(data, path_and_name):
    hdu = fits.PrimaryHDU(data)
    hdul = fits.HDUList([hdu])
    hdul.writeto(path_and_name)


def get_real_data(path, model):
    fits_file = fits.open(os.path.join(path))
    fits_file.info()
    data = fits_file[0].data
    fits_file.close()
    start = 0
    end = model.image.target.shape[0]
    return data[start:end, :, :]

def inseart_planet(im_star, pos, br):
    time_space = im_star.domain[0]
    pos_space = im_star.domain[1]
    k_space = pos_space.get_default_codomain()
    pos_dom = im_star.domain
    k_dom = ift.DomainTuple.make((time_space, k_space))
    FFT_image = ift.FFTOperator(k_dom, space=1)
    delta = np.zeros(im_star.shape)
    delta[:,pos[0],pos[1]] = br/pos_space.dvol
    delta = ift.Field.from_raw(pos_dom, delta)
    
    rlz = ift.Realizer(FFT_image.target)
    delta_fs = FFT_image.inverse(delta)
    im_star_fs = FFT_image.inverse(im_star)
    im_planet = rlz(FFT_image(delta_fs*im_star_fs))
    return im_star + im_planet


def get_synthentic_data(configpars, readout_std):
    model = HighContrasImaging(configpars, 0, 0)
    contrast = 0.008
    pos = [20,20]
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    shp = model.image_star.target.shape
    grd = np.full(shp[1:3], 1e-10)
    grd[50,50] = 1.
    grd[50+pos[0],50+pos[1]] = contrast
    plt.imshow(grd, origin='lower', norm=LogNorm(vmin=5e-4), cmap='hot')
    plt.colorbar(label='relative brightness')
    plt.title('ground truth object')
    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.tight_layout(pad=0.1, h_pad=0.1, w_pad=0.1)
    plt.savefig('ground_truth_synthetic.png')
    plt.close()
    try:
        data = get_real_data("synthetic.fits", model)
    except:
        mock = ift.from_random(model.image_star.domain)
        im_star = model.image_star(mock)
        im = inseart_planet(im_star, pos, contrast).val
        data = np.random.poisson(im)
        data = np.random.normal(loc=data,scale=readout_std)
        if master:
            dump_data(data, "synthetic.fits")
        data = get_real_data("synthetic.fits", model)
    plt.imshow(np.where(data<1e-5, 1e-5, data)[4,:], origin='lower', norm=LogNorm(vmin=1), cmap='hot')
    plt.title('data frame')
    plt.colorbar(label='counts')
    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.tight_layout(pad=0.1, h_pad=0.1, w_pad=0.1)
    plt.savefig('data_frame.png')
    plt.close()
    data = ift.makeField(model.image.target, data)
    return data

