# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


def rotate(origin, points, angle):
    ox, oy = origin
    px, py = points

    qx = ox + np.cos(angle) * (px - ox) - np.sin(angle) * (py - oy)
    qy = oy + np.sin(angle) * (px - ox) + np.cos(angle) * (py - oy)
    return qx, qy


def get_rotated_coordinates(domain, time_pix, rotation_speed_per_frame):
    shp = domain.shape
    dist = domain[0].distances

    points = np.mgrid[0 : dist[0] * shp[0] : dist[0], 0 : dist[1] * shp[1] : dist[1]]
    points = np.dstack(points)
    points = np.reshape(points, (shp[0] * shp[0], 2)).T

    origin = [0.5 * shp[0] * dist[0], 0.5 * shp[1] * dist[1]]
    rot = rotate(origin, points, 1e-9)

    rot_points = np.empty((2, shp[0] * shp[1] * time_pix))
    for i in range(time_pix):
        rot = rotate(origin, points, rotation_speed_per_frame * i)
        start = i * shp[0] * shp[1]
        end = (i + 1) * shp[0] * shp[1]
        rot_points[:, start:end] = rot

    return rot_points


def get_rotated_coordinates3d(domain, rotation_speed_per_frame):
    shp = domain[1].shape
    dist = domain[1].distances

    points = np.mgrid[0 : dist[0] * shp[0] : dist[0], 0 : dist[1] * shp[1] : dist[1]]
    points = np.dstack(points)
    points = np.reshape(points, (shp[0] * shp[0], 2)).T

    origin = [0.5 * shp[0] * dist[0], 0.5 * shp[1] * dist[1]]
    rot = rotate(origin, points, 1e-9)

    tpix = domain[0].shape[0]
    tdist = domain[0].distances[0]
    rot_points = np.empty((3, shp[0] * shp[1] * tpix))
    for i in range(tpix):
        rot = rotate(origin, points, rotation_speed_per_frame * i)
        start = i * shp[0] * shp[1]
        end = (i + 1) * shp[0] * shp[1]
        rot_points[1:3, start:end] = rot
        rot_points[0, start:end] = i * tdist

    return rot_points


class DCH(ift.LinearOperator):
    def __init__(self, domain, target):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return ift.Field.from_raw(self._target, x.val)
        else:
            return ift.Field.from_raw(self._domain, x.val)


class Reshape(ift.LinearOperator):
    def __init__(self, domain, target):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return ift.Field.from_raw(self._target, x.val.reshape(self._target.shape))
        else:
            return ift.Field.from_raw(self._domain, x.val.reshape(self._domain.shape))


class FieldRotation(ift.Operator):
    def __init__(self, domain, time_dom, rot_per_frame):
        self._domain = ift.makeDomain(domain)
        time_dom = ift.makeDomain(time_dom)
        self._target = ift.makeDomain((time_dom[0], self._domain[0]))
        points = get_rotated_coordinates(domain, time_dom.shape[0], rot_per_frame)
        inter = ift.LinearInterpolator(domain, points)
        rshp = Reshape(inter.target, self._target)
        self._op = rshp @ inter

    def apply(self, x):
        return self._op(x)


class DomainChanger(ift.LinearOperator):
    def __init__(self, domain, target):
        self._domain = ift.makeDomain(domain)
        self._target = ift.DomainTuple.make(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return ift.Field.from_raw(self._target, x.val)
        else:
            return ift.Field.from_raw(self._domain, x.val)


class AtmRotation(ift.Operator):
    def __init__(self, domain, rot_per_frame):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(domain)
        points = get_rotated_coordinates3d(domain, rot_per_frame)
        t_dist = self._domain[0].distances[0]
        x_dist = self._domain[1].distances[1]
        int_dst = [t_dist, x_dist, x_dist]
        shp = self._domain.shape
        int_dom = ift.RGSpace(shp, int_dst)
        dch = DomainChanger(self._domain, int_dom)
        inter = ift.LinearInterpolator(dch.target, points)
        rshp = Reshape(inter.target, dch.domain)

        self._op = rshp @ inter @ dch

    def apply(self, x):
        return self._op(x)
