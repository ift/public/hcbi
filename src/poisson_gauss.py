# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


def _H_d_s_gauss(d, s, N):
    spn = s + N
    smd = s - d
    res = 0.5 * np.log(2 * np.pi * spn) + 0.5 * smd * smd / spn
    return res


def _del_H_del_s_gauss(d, s, N):
    spn = np.abs(s) + N
    smd = s - d
    res = 0.5 + smd - 0.5 * smd * smd / spn
    res /= spn
    return res


def _M_s_gauss(s, N):
    spn = np.abs(s) + N
    return (1.0 / spn) + (0.5 / (spn * spn))


class PoissonGaussApproxEnergy(ift.EnergyOperator):
    def __init__(self, data, N, domain):
        self._data = data
        self._N = N
        self._domain = ift.makeDomain(domain)
        self._sampling_dtype = data.val.dtype

    def apply(self, x):
        self._check_input(x)
        xval = x.val
        lin = isinstance(x, ift.Linearization)
        if lin:
            xval = x.val.val

        res = np.sum(_H_d_s_gauss(self._data.val, xval, self._N))
        res = ift.Field.scalar(res)
        if not lin:
            return res
        del_s = _del_H_del_s_gauss(self._data.val, xval, self._N)
        del_s = ift.Field.from_raw(x.val.domain, del_s)
        contr = ift.ContractionOperator(x.val.domain, None)
        jac = contr @ ift.DiagonalOperator(del_s)
        lin_res = x.new(res, jac)
        if not x.want_metric:
            return lin_res
        mtr = _M_s_gauss(xval, self._N)
        mtr = ift.Field.from_raw(self._domain, mtr)
        metric = ift.makeOp(mtr)
        return lin_res.add_metric(ift.SamplingDtypeSetter(metric, self._sampling_dtype))
