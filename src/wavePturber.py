# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


class wavePtb(ift.Operator):
    def __init__(self, wave, ptb_dom, ptb_key, flux_dom, flux_key):
        self._domain = ift.MultiDomain.make({ptb_key: ptb_dom, flux_key: flux_dom})
        self._target = ift.makeDomain(ptb_dom)
        self._ptb_key = ptb_key
        self._wave = wave
        self._ptb_dom = ptb_dom
        self._flux_dom = flux_dom
        self._flux_key = flux_key

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val
        wv = self._wave
        pt = xval[self._ptb_key]
        fl = xval[self._flux_key]
        res = np.einsum("jk,ijk,i->ijk", wv.val, pt.val, fl.val)
        res = ift.Field.from_raw(self._target, res)
        if not lin:
            return res

        dFl = dF(self._flux_dom, wv, pt).ducktape(self._flux_key)
        dPt = dP(self._ptb_dom, wv, fl).ducktape(self._ptb_key)
        return x.new(res, (dFl + dPt))


class dF(ift.LinearOperator):
    def __init__(self, domain, wv, ptb):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(ptb.domain)
        self._wv = wv.val
        self._ptb = ptb.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            res = np.einsum("jk,ijk,i->ijk", self._wv, self._ptb, x.val)
            res = ift.makeField(self._target, res)
            return res
        if mode == self.ADJOINT_TIMES:
            res = np.einsum(
                "jk,ijk,ijk->i", self._wv.conjugate(), self._ptb.conjugate(), x.val
            )
            res = ift.makeField(self._domain, res)
            return res


class dP(ift.LinearOperator):
    def __init__(self, domain, wv, fl):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(domain)
        self._wv = wv.val
        self._fl = fl.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            res = np.einsum("jk,i,ijk->ijk", self._wv, self._fl, x.val)
            res = ift.makeField(self._target, res)
            return res
        if mode == self.ADJOINT_TIMES:
            res = np.einsum(
                "jk,i,ijk->ijk", self._wv.conjugate(), self._fl.conjugate(), x.val
            )
            res = ift.makeField(self._domain, res)
            return res
