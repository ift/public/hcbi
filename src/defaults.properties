# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

[data_args]
seed_offset = 42

[pixel_args]
sensor_mask = 0.5
time_pix_dist = 100
sensor_pix_dist = 12.
time_mask = 30
field_rot = 0
atm_rot = 0
cut_rows = 0
derot=0


[brightness_args]
activate_star=1
planets = 1
lognormal_planets=0
star = {'mean': 2e-7,
		'sig': 1e-7,
		'key': 'star'}
pts = {'alpha': 1.03,
	   'q': 5e-8,
	   'delta': 3e-5}
pts_lognormal = {'mean': 3e-10,
				 'sig': 3e-10,
				 'key': 'planets_lognormal'}
back=0
correl_back=0
back_params = {'alpha': 1.03,
	   'q': 5e-6,
	   'delta': 3e-5}
mask_fraction=0.06

[model_args]
path_length=double
gradient_fields = 0
start_learn_gradients=0
learn_gradients_until=0
fix_gradients=0
readout_std=0
mgvi_samples=2
psf_two_change_weight=1 
psf_two_weights=[0.7, 0.7, 0.7, 0.7, 0.4, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
keep_fixed_until=0
only_learn_gradients=['correl_back_asperity', 'correl_back_flexibility', 'correl_back_fluctuations', 'correl_back_loglogavgslope', 'correl_back_spectrum', 'correl_back_xi', 'correl_back_zeromode', 'gaus_intensity', 'gaus_intensity_back', 'phase1_pos_cutoff', 'phase1_pos_logloghalfslope', 'phase1_pos_scale', 'phase1_time_cutoff', 'phase1_time_logloghalfslope', 'phase1_time_scale', 'phase1_xi', 'phase1_zeromode', 'phase2_pos_cutoff', 'phase2_pos_logloghalfslope', 'phase2_pos_scale', 'phase2_time_cutoff', 'phase2_time_logloghalfslope', 'phase2_time_scale', 'phase2_xi', 'phase2_zeromode', 'phase1_pos_asperity', 'phase1_pos_flexibility', 'phase1_pos_fluctuations', 'phase1_pos_loglogavgslope', 'phase1_pos_spectrum', 'phase1_time_asperity', 'phase1_time_flexibility', 'phase1_time_fluctuations', 'phase1_time_loglogavgslope', 'phase1_time_spectrum', 'phase2_pos_asperity', 'phase2_pos_flexibility', 'phase2_pos_fluctuations', 'phase2_pos_loglogavgslope', 'phase2_pos_spectrum', 'phase2_time_asperity', 'phase2_time_flexibility', 'phase2_time_fluctuations', 'phase2_time_loglogavgslope', 'phase2_time_spectrum', 'phase1_xi', 'phase2_xi', 'distance']
key_fixed=['phase1_pos_asperity', 'phase1_pos_flexibility', 'phase1_pos_fluctuations', 'phase1_pos_loglogavgslope', 'phase1_pos_spectrum', 'phase1_time_asperity', 'phase1_time_flexibility', 'phase1_time_fluctuations', 'phase1_time_loglogavgslope', 'phase1_time_spectrum', 'phase2_pos_asperity', 'phase2_pos_flexibility', 'phase2_pos_fluctuations', 'phase2_pos_loglogavgslope', 'phase2_pos_spectrum', 'phase2_time_asperity', 'phase2_time_flexibility', 'phase2_time_fluctuations', 'phase2_time_loglogavgslope', 'phase2_time_spectrum', 'phase1_pos_cutoff', 'phase1_pos_fluctuations', 'phase1_pos_scale', 'phase1_pos_logloghalfslope',  'phase1_pos_loglogavgslope', 'phase1_time_cutoff', 'phase1_time_scale', 'phase1_time_logloghalfslope', 'phase1_time_fluctuations', 'phase1_time_loglogavgslope','phase1_zeromode', 'gaus_intensity']
matern_path_length=1
shift_keys_one=['x_shiftasperity', 'x_shiftflexibility', 'x_shiftfluctuations', 'x_shiftloglogavgslope', 'x_shiftspectrum', 'x_shiftxi', 'x_shiftzeromode', 'y_shiftasperity', 'y_shiftflexibility', 'y_shiftfluctuations', 'y_shiftloglogavgslope', 'y_shiftspectrum', 'y_shiftxi', 'y_shiftzeromode']
shift_keys_two=['x_shift2asperity', 'x_shift2flexibility', 'x_shift2fluctuations', 'x_shift2loglogavgslope', 'x_shift2spectrum', 'x_shift2xi', 'x_shift2zeromode', 'y_shift2asperity', 'y_shift2flexibility', 'y_shift2fluctuations', 'y_shift2loglogavgslope', 'y_shift2spectrum', 'y_shift2xi', 'y_shift2zeromode']



[phase_screen]
cfMaker_make = {'prefix': 'phase1_'}
cfMaker_fl_time = {'fluctuations': (25.0, 10.1),
				   'flexibility': (0.4,0.1),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-2.2, 0.1),
				   'prefix': 'time_'}
cfMaker_fl_pos = {'fluctuations': (1.5,0.2),
			  'flexibility': (0.4,0.1),
			  'asperity': (0.1,0.05),
			  'loglogavgslope': (-2.2,0.1),
			  'prefix': 'pos_'}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (7.0, 3.0)}
cfMaker_final = {'prior_info':0}

[phase_screen_matern]
mkMaker_make = {'offset_mean': 0.0,
				'offset_std': (7.0, 3.0),
				'prefix': 'phase1_'}
mkMaker_fl_time = {'scale': (200.0, 80.0),
				   'cutoff': (5e-4, 2e-4),
				   'logloghalfslope': (1.7, 0.3),
				   'prefix': 'time_'}
mkMaker_fl_pos = {'scale': (0.03, .001),
				   'cutoff': (40, 20),
				   'logloghalfslope': (1.7, 0.5),
				   'prefix': 'pos_'}
mkMaker_final = {}


[phase_screen_two]
cfMaker_make = {'prefix': 'phase2_'}
cfMaker_fl_time = {'fluctuations': (25.0,10.),
				   'flexibility': (0.4,0.1),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-1.3,0.01),
				   'prefix': 'time_'}
cfMaker_fl_pos = {'fluctuations': (1.5,0.05),
			  'flexibility': (0.4,0.1),
			  'asperity': (0.1,0.05),
			  'loglogavgslope': (-1.3,0.1),
			  'prefix': 'pos_'}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (7.0, 3.0)}
cfMaker_final = {'prior_info':0}

[phase_screen_matern_two]
mkMaker_make = {'offset_mean': 0.0,
				'offset_std': (7.0, 3.0),
				'prefix': 'phase2_'}
mkMaker_fl_time = {'scale': (200.0, 80.0),
				   'cutoff': (5e-4, 1.5e-4),
				   'logloghalfslope': (0.5, 0.25),
				   'prefix': 'time_'}
mkMaker_fl_pos = {'scale': (0.03, 0.01),
				   'cutoff': (40., 10.),
				   'logloghalfslope': (0.5, 0.25),
				   'prefix': 'pos_'}
mkMaker_final = {}

[flux_scale]
cfMaker_make = {'prefix': 'flux_'}
cfMaker_fl_time = {'fluctuations': (0.3,0.1),
				   'flexibility': (1.0,0.5),
				   'asperity': (0.1,0.5),
				   'loglogavgslope': (-3,0.5),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': 1.0,
				'offset_std': (0.01, 0.01)}
cfMaker_final = {'prior_info':0}


[correl_back_conf]
cfMaker_make = {'prefix': 'correl_back_'}
cfMaker_fl = {'fluctuations': (1.,0.3),
				   'flexibility': (1.0,0.5),
				   'asperit': (0.1,0.05),
				   'loglogavgslope': (-6,0.05),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': -13.0,
				'offset_std': (3., 0.1)}
cfMaker_final = {'prior_info':0}

[shiftX]
cfMaker_make = {'prefix': 'x_shift'}
cfMaker_fl_time = {'fluctuations': (30.,20.),
				   'flexibility': (1.0,0.5),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-3,0.5),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (30., 20.0)}
cfMaker_final = {'prior_info':0}

[shiftX_two]
cfMaker_make = {'prefix': 'x_shift2'}
cfMaker_fl_time = {'fluctuations': (30.,20.),
				   'flexibility': (1.0,0.5),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-3,0.5),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (30., 20.0)}
cfMaker_final = {'prior_info':0}


[shiftY]
cfMaker_make = {'prefix': 'y_shift'}
cfMaker_fl_time = {'fluctuations': (30.,20.),
				   'flexibility': (1.0,0.5),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-3,0.5),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (30., 20.0)}
cfMaker_final = {'prior_info':0}

[shiftY_two]
cfMaker_make = {'prefix': 'y_shift2'}
cfMaker_fl_time = {'fluctuations': (30.,20.),
				   'flexibility': (1.0,0.5),
				   'asperity': (0.1,0.05),
				   'loglogavgslope': (-3,0.5),
				   'prefix': ''}
cfMaker_offset = {'offset_mean': 0.0,
				'offset_std': (30., 20.0)}
cfMaker_final = {'prior_info':0}
