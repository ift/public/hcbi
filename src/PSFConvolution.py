# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


class PSFconv(ift.Operator):
    def __init__(self, wave_dom, wave_key, PSF_dom, PSF_key):
        self._domain = ift.MultiDomain.make({wave_key: wave_dom, PSF_key: PSF_dom})
        self._target = ift.makeDomain(PSF_dom)
        self._wave_key = wave_key
        self._psf_key = PSF_key
        self._wave_dom = wave_dom
        self._psf_dom = PSF_dom

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val
        wv = xval[self._wave_key]
        psf = xval[self._psf_key]
        res = np.einsum("jk,ijk->ijk", wv.val, psf.val)
        res = ift.Field.from_raw(self._target, res)
        if not lin:
            return res

        dPf = dPSF(self._psf_dom, wv).ducktape(self._psf_key)
        dWv = dW(self._wave_dom, psf).ducktape(self._wave_key)
        return x.new(res, (dPf + dWv))


class dPSF(ift.LinearOperator):
    def __init__(self, domain, wv):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(domain)
        self._wv = wv.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            res = np.einsum("jk,ijk->ijk", self._wv, x.val)
            res = ift.makeField(self._target, res)
            return res
        if mode == self.ADJOINT_TIMES:
            res = np.einsum("jk,ijk->ijk", self._wv.conjugate(), x.val)
            res = ift.makeField(self._domain, res)
            return res


class dW(ift.LinearOperator):
    def __init__(self, domain, psf):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(psf.domain)
        self._ptb = psf.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            res = np.einsum("ijk,jk->ijk", self._ptb, x.val)
            res = ift.makeField(self._target, res)
            return res
        if mode == self.ADJOINT_TIMES:
            res = np.einsum("ijk,ijk->jk", self._ptb.conjugate(), x.val)
            res = ift.makeField(self._domain, res)
            return res
