# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import ast

import src.matern_kernel as kernel


class PathLength:
    def __init__(self, configpars):
        pixel_conf = dict(configpars.items("pixel_args"))
        sensor_pix_x = int(pixel_conf["sensor_pix_x"])
        sensor_pix_y = int(pixel_conf["sensor_pix_y"])
        sensor_pix_dist = float(pixel_conf["sensor_pix_dist"])
        sensors_mask = float(pixel_conf["sensor_mask"])
        time_pix = int(pixel_conf["time_pix"])
        time_pix_dist = float(pixel_conf["time_pix_dist"])
        time_mask = float(pixel_conf["time_mask"])

        model_conf = dict(configpars.items("model_args"))
        matern_path_length = int(model_conf["matern_path_length"])

        pos_pix_x = int(sensor_pix_x + sensors_mask * sensor_pix_x)
        pos_pix_y = int(sensor_pix_y + sensors_mask * sensor_pix_y)

        harmonic_space = ift.RGSpace(
            [pos_pix_x, pos_pix_y], distances=sensor_pix_dist, harmonic=True
        )
        position_space = harmonic_space.get_default_codomain()

        time_space = ift.RGSpace(time_pix + time_mask, distances=time_pix_dist)

        if matern_path_length == 1:
            matern_kernel_conf = dict(configpars.items("phase_screen_matern"))
            mkmaker = kernel.CorrelatedFieldMaker.make(
                **ast.literal_eval(matern_kernel_conf["mkmaker_make"])
            )
            mkmaker.add_fluctuations_matern(
                time_space, **ast.literal_eval(matern_kernel_conf["mkmaker_fl_time"])
            )
            mkmaker.add_fluctuations_matern(
                position_space, **ast.literal_eval(matern_kernel_conf["mkmaker_fl_pos"])
            )
            self.phase_screen_one = mkmaker.finalize(
                **ast.literal_eval(matern_kernel_conf["mkmaker_final"])
            )

            matern_kernel_conf_two = dict(configpars.items("phase_screen_matern_two"))
            mkmaker_two = kernel.CorrelatedFieldMaker.make(
                **ast.literal_eval(matern_kernel_conf_two["mkmaker_make"])
            )
            mkmaker_two.add_fluctuations_matern(
                time_space,
                **ast.literal_eval(matern_kernel_conf_two["mkmaker_fl_time"])
            )
            mkmaker_two.add_fluctuations_matern(
                position_space,
                **ast.literal_eval(matern_kernel_conf_two["mkmaker_fl_pos"])
            )
            self.phase_screen_two = mkmaker_two.finalize(
                **ast.literal_eval(matern_kernel_conf_two["mkmaker_final"])
            )

        else:

            phase_conf = dict(configpars.items("phase_screen"))
            cfmaker = ift.CorrelatedFieldMaker(
                **ast.literal_eval(phase_conf["cfmaker_make"])
            )
            cfmaker.add_fluctuations(
                time_space, **ast.literal_eval(phase_conf["cfmaker_fl_time"])
            )
            cfmaker.add_fluctuations(
                position_space, **ast.literal_eval(phase_conf["cfmaker_fl_pos"])
            )
            cfmaker.set_amplitude_total_offset(
                    **ast.literal_eval(phase_conf["cfmaker_offset"])
            )
            self.phase_screen_one = cfmaker.finalize(
                **ast.literal_eval(phase_conf["cfmaker_final"])
            )
            self.A_time_one = cfmaker.get_normalized_amplitudes()[0]
            self.A_space_one = cfmaker.get_normalized_amplitudes()[1]
            self.norm_one = cfmaker.amplitude_total_offset

            phase_conf_two = dict(configpars.items("phase_screen_two"))
            cfmaker_two = ift.CorrelatedFieldMaker(
                **ast.literal_eval(phase_conf_two["cfmaker_make"])
            )
            cfmaker_two.add_fluctuations(
                time_space, **ast.literal_eval(phase_conf_two["cfmaker_fl_time"])
            )
            cfmaker_two.add_fluctuations(
                position_space, **ast.literal_eval(phase_conf_two["cfmaker_fl_pos"])
            )
            cfmaker_two.set_amplitude_total_offset(
                    **ast.literal_eval(phase_conf_two["cfmaker_offset"])
            )
            self.phase_screen_two = cfmaker_two.finalize(
                **ast.literal_eval(phase_conf_two["cfmaker_final"])
            )
            self.A_time_two = cfmaker_two.get_normalized_amplitudes()[0]
            self.A_space_two = cfmaker_two.get_normalized_amplitudes()[1]
            self.norm_two = cfmaker_two.amplitude_total_offset
