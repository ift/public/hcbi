# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np

from src.psf import PSF
from src.object_brightness import ObjectBrightness
from src.PSFConvolution import PSFconv
from src.GeometricMask import GeomMaskOperator
from src.domChanger import DomainChanger
from src.row_cut import Cut_first_last_row
from src.rotate import FieldRotation
from src.rotate import AtmRotation


class HighContrasImaging:
    def __init__(self, configpars, psf_two_weight=1, start_learn_gradients=0):
        pixel_conf = dict(configpars.items("pixel_args"))
        sensor_pix_x = int(pixel_conf["sensor_pix_x"])
        sensor_pix_y = int(pixel_conf["sensor_pix_y"])
        sensor_pix_dist = float(pixel_conf["sensor_pix_dist"])
        sensors_mask = float(pixel_conf["sensor_mask"])
        time_pix = int(pixel_conf["time_pix"])
        time_pix_dist = float(pixel_conf["time_pix_dist"])
        time_mask = float(pixel_conf["time_mask"])
        field_rot = float(pixel_conf["field_rot"])
        atm_rot = float(pixel_conf["atm_rot"])
        cut_rows = int(pixel_conf["cut_rows"])

        model_args = dict(configpars.items("model_args"))

        brigthness_args = dict(configpars.items("brightness_args"))
        planets = int(brigthness_args["planets"])

        pos_pix_x = int(sensor_pix_x + sensors_mask * sensor_pix_x)
        pos_pix_y = int(sensor_pix_y + sensors_mask * sensor_pix_y)

        harmonic_space = ift.RGSpace(
            [pos_pix_x, pos_pix_y], distances=sensor_pix_dist, harmonic=True
        )
        harmonic_space_obs = ift.RGSpace(
            [sensor_pix_x, sensor_pix_y], distances=sensor_pix_dist, harmonic=True
        )

        time_space = ift.RGSpace(time_pix + time_mask, distances=time_pix_dist)
        time_space_obs = ift.RGSpace(time_pix, distances=time_pix_dist)

        obs_sp = ift.DomainTuple.make((time_space_obs, harmonic_space_obs))
        harmonic_space_tuple = ift.DomainTuple.make((time_space, harmonic_space))

        FFT_image = ift.FFTOperator(harmonic_space_tuple, space=1)

        self.point_spread_function = PSF(configpars, psf_two_weight)
        self.object = ObjectBrightness(configpars)
        rlz = ift.Realizer(self.point_spread_function.psf.target)

        harmonic_space_psf = FFT_image @ rlz.adjoint @ self.point_spread_function.psf
        if field_rot == 0.0:
            conv = PSFconv(
                self.object.object_intensity.target,
                "objectInt",
                harmonic_space_psf.target,
                "psf",
            )

            mf_psf = ift.FieldAdapter(harmonic_space_psf.target, "psf")
            mf_object = ift.FieldAdapter(
                self.object.object_intensity.target, "objectInt"
            )
            conv_mf = mf_psf.adjoint(harmonic_space_psf) + mf_object.adjoint(
                self.object.object_intensity
            )
            conv_mf_star = mf_psf.adjoint(harmonic_space_psf) + mf_object.adjoint(
                self.object.star
            )
            if planets == 1:
                conv_mf_pts = mf_psf.adjoint(harmonic_space_psf) + mf_object.adjoint(
                    self.object.pts_waves
                )

            sensors_mask = GeomMaskOperator(FFT_image.domain, obs_sp)
            image_rls = ift.Realizer(sensors_mask.target)

            image = (
                image_rls @ sensors_mask @ FFT_image.inverse(conv(conv_mf))
            ).absolute()
            self.image_star = (
                image_rls @ sensors_mask @ FFT_image.inverse(conv(conv_mf_star))
            ).absolute()
            if planets == 1:
                self.image_planet = (
                    image_rls @ sensors_mask @ FFT_image.inverse(conv(conv_mf_pts))
                ).absolute()
        else:
            FFT = ift.FFTOperator(harmonic_space)
            obj = FFT.inverse(self.object.object_intensity)
            object_rot = FieldRotation(obj.target, time_space, field_rot)
            conv = FFT_image(object_rot @ obj) * harmonic_space_psf
            sensors_mask = GeomMaskOperator(FFT_image.domain, obs_sp)
            image_rls = ift.Realizer(sensors_mask.target)
            image = image_rls @ sensors_mask @ FFT_image.inverse(conv)

        if atm_rot != 0:
            rot = AtmRotation(image.target, atm_rot)
            image = rot @ image

        dch = DomainChanger(
            image.target, (time_space_obs, harmonic_space_obs.get_default_codomain())
        )
        smooth = ift.HarmonicSmoothingOperator(dch.target, 0.004, space=1)
        self.smooth = dch.adjoint @ smooth @ dch

        if start_learn_gradients == 1:
            self.image_full = self.smooth @ image
        else:
            self.image_full = image

        if cut_rows == 1:
            self.cut = Cut_first_last_row(self.image_full.target)
            self.image = self.cut @ self.image_full
        else:
            self.image = self.image_full
