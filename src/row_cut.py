# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np


class Cut_first_last_row(ift.LinearOperator):
    def __init__(self, domain):
        self._domain = ift.makeDomain(domain)
        shp = self._domain.shape
        target_shape_pos = [shp[1] - 4, shp[2]]
        target_pos_space = ift.RGSpace(target_shape_pos, domain[1].distances)
        self._target = ift.DomainTuple.make([domain[0], target_pos_space])
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            return ift.Field.from_raw(self._target, x.val[:, 2:-2, :])
        else:
            res = np.zeros(self._domain.shape)
            res[:, 2:-2, :] = x.val
            return ift.Field.from_raw(self._domain, res)
