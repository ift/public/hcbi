# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import numpy as np
import nifty7 as ift


class GeomMaskOperator(ift.LinearOperator):
    """
    Takes a field and extracts the central part of the field corresponding to target.shape

    Parameters
    ----------
    domain : Domain, DomainTuple or tuple of Domain
        The operator's input domain.
    target : Domain, DomainTuple or tuple of Domain
        The operator's target domain
    """

    def __init__(self, domain, target):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        sl = []
        for i in range(len(self._domain.shape)):
            slStart = int((self._domain.shape[i] - self._target.shape[i]) / 2.0)
            slStop = slStart + self._target.shape[i]
            sl.append(slice(slStart, slStop, 1))
        self._slices = tuple(sl)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val
        if mode == self.TIMES:
            res = x[self._slices]
            return ift.Field(self.target, res)
        res = np.zeros(self.domain.shape, x.dtype)
        res[self._slices] = x
        return ift.Field(self.domain, res)
