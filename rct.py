# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import os
import configparser
import numpy as np
import pickle
import sys
import ast
from shutil import copy

from src.getData import get_real_data
from src.getData import get_synthentic_data
from src.high_contrast_imaging import HighContrasImaging
from src.poisson_gauss import PoissonGaussApproxEnergy
from mkplt import generate_plots


#np.seterr(divide="raise", over="raise", invalid="raise")

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    n_task = comm.Get_size()
    rank = comm.Get_rank()
except ImportError:

    comm = None
    n_task = 1
    rank = 0

master = rank == 0

def main(config, iteration):
    def built_H(configpars, weight=1, start_learn_gradients=0, cut_rows=0, derot=0):
        model = HighContrasImaging(configpars, weight, start_learn_gradients)
        if data_path == 'synthetic.fits':
            data = get_synthentic_data(configpars, readout_std)
        else:
            data = get_real_data(data_path, model)
            data = ift.Field.from_raw(model.image_full.target, data)
        if cut_rows == 1:
            data = model.cut(data)
        if derot == 1:
            mask = np.zeros_like(data.val)
            mask = np.where(np.isfinite(data.val), 0, 1)
            mask = ift.Field.from_raw(data.domain, mask)
            mask_op = ift.MaskOperator(mask)
            data_mask = mask_op(data)
        if readout_std > 0:
            var = ift.Field.from_raw(model.image.target, readout_std**2)
            if derot == 1:
                likelihood = (
                    PoissonGaussApproxEnergy(
                        data_mask, mask_op(var).val, mask_op.target
                    )
                    @ mask_op
                    @ model.image
                )
            else:
                likelihood = (
                    PoissonGaussApproxEnergy(data, var.val, model.image.target)
                    @ model.image
                )
        elif readout_std == 0:
            if derot == 1:
                data_mask = np.where(data_mask.val > 0, data_mask.val, 0)
                data_mask = data_mask.astype(int)
                data_mask = ift.Field.from_raw(mask_op.target, data_mask)
            else:
                data = np.where(data.val > 0, data.val, 0)
                data = data.astype(int)
                data = ift.Field.from_raw(model.image.target, data)
            if derot == 1:
                likelihood = ift.PoissonianEnergy(data_mask)(mask_op @ model.image)
            else:
                likelihood = ift.PoissonianEnergy(data)(model.image)
        else:
            raise RuntimeError("readout_std must be >= 0")

        ic_sampling = ift.AbsDeltaEnergyController(
            name="Sampling", deltaE=0.05, iteration_limit=250
        )
        return ift.StandardHamiltonian(likelihood, ic_sampling)

    configpars = configparser.RawConfigParser()
    configpars.read(["src/defaults.properties", config])


    data_conf = dict(configpars.items("data_args"))
    data_path = data_conf["data_path"]
    out_path = data_conf["output_path"]
    seed_offset = int(data_conf["seed_offset"])

    model_args = dict(configpars.items("model_args"))
    start_learn_gradients = int(model_args["start_learn_gradients"])
    readout_std = float(model_args["readout_std"])
    psf_two_change_weight = int(model_args["psf_two_change_weight"])
    psf_two_weights = ast.literal_eval(model_args["psf_two_weights"])
    path_length = model_args["path_length"]
    learn_gradients_until = int(model_args["learn_gradients_until"])
    fix_gradients = int(model_args["fix_gradients"])
    keep_fixed_until = int(model_args["keep_fixed_until"])
    only_learn_gradients = ast.literal_eval(model_args["only_learn_gradients"])
    fixed_keys = ast.literal_eval(model_args["key_fixed"])
    shift_keys_one = ast.literal_eval(model_args["shift_keys_one"])
    shift_keys_two = ast.literal_eval(model_args["shift_keys_two"])
    path_length_model = model_args["path_length"]

    pixel_args = dict(configpars.items("pixel_args"))
    cut_rows = int(pixel_args["cut_rows"])
    derot = int(pixel_args["derot"])

    ift.random.push_sseq_from_seed(42 + seed_offset + iteration)


    if master:
        try:
            os.mkdir(os.path.normpath(out_path))
        except OSError:
            pass
        copy("config.properties", os.path.normpath(out_path))

    H_nru = built_H(configpars, cut_rows=cut_rows, derot=derot)

    samp_fl_name = out_path + "/KL_samp_it_{iteration}.p"
    pos_fl_name = out_path + "/KL_pos_it_{iteration}.p"

    if (start_learn_gradients == 1) and (iteration == 0):
        initial_mean = 1e-2 * ift.from_random(H_nru.domain, "normal")
    elif (start_learn_gradients == 1) and (iteration - 1 < learn_gradients_until):
        name = pos_fl_name.format(iteration=iteration - 1)
        name = os.path.normpath(name)
        shift_mean_np = pickle.load(open(name, "rb")).val
        initial_mean = 1e-2 * ift.from_random(H_nru.domain, "normal")
        mean_np = initial_mean.val_rw()
        if path_length == "double":
            if not len(shift_keys_one) == len(shift_keys_two):
                raise RuntimeError("different length")
            for i in range(len(shift_keys_one)):
                k_one = shift_keys_one[i]
                k_two = shift_keys_two[i]
                mean_np[k_one] = shift_mean_np[k_one]
                mean_np[k_two] = shift_mean_np[k_one]
            initial_mean = ift.MultiField.from_raw(initial_mean.domain, mean_np)
        elif path_length == "single":
            for i in range(len(shift_keys_one)):
                k_one = shift_keys_one[i]
                mean_np[k_one] = shift_mean_np[k_one]
            initial_mean = ift.MultiField.from_raw(initial_mean.domain, mean_np)
        else:
            raise NotImplementedError("Not Implemented (so fare)")
    elif iteration > 0:
        name = pos_fl_name.format(iteration=iteration - 1)
        name = os.path.normpath(name)
        initial_mean = pickle.load(open(name, "rb"))
    else:
        initial_mean = 1e-2 * ift.from_random(H_nru.domain, "normal")
        keys = initial_mean.keys()
        initial_mean = initial_mean.val_rw()
        zero_keys = ['phase1_xi', 'phase2_xi']
        for kk in zero_keys:
            if kk in keys:
                print('set to zero: '+kk)
                initial_mean[kk] = np.full_like(initial_mean[kk], 0.)
        initial_mean = ift.makeField(H_nru.domain, initial_mean)

    if iteration < 1:
        ic_newton = ift.AbsDeltaEnergyController(name="Newton", deltaE=0.05, iteration_limit=10)
    elif iteration < 2:
        ic_newton = ift.AbsDeltaEnergyController(name="Newton", deltaE=0.05, iteration_limit=5)
    elif iteration < 3:
        ic_newton = ift.AbsDeltaEnergyController(name="Newton", deltaE=0.05, iteration_limit=10)
    elif iteration < 10:
        ic_newton = ift.AbsDeltaEnergyController(name="Newton", deltaE=0.05, iteration_limit=15)
    else:
        ic_newton = ift.AbsDeltaEnergyController(name="Newton", deltaE=0.05, iteration_limit=35)
    minimizer = ift.NewtonCG(ic_newton)
    kwargs = {}

    if (start_learn_gradients == 1) and (iteration < learn_gradients_until):
        H = built_H(
            configpars, 0.0, start_learn_gradients=1, cut_rows=cut_rows, derot=derot
        )
        N_samples = 2
        initial_mean = initial_mean.extract(H.domain)

        kwargs["constants"] = only_learn_gradients
        if n_task != 1:
            kwargs["comm"] = comm

    else:
        N_samples = int(model_args["mgvi_samples"])
        H_kwargs = {
            "configpars": configpars,
            "weight": psf_two_weights[iteration],
            "cut_rows": cut_rows,
            "derot": derot,
        }
        if iteration < learn_gradients_until:
            H_kwargs["start_learn_gradients"] = 1
        else:
            H_kwargs["start_learn_gradients"] = 0
        H = built_H(**H_kwargs)
        initial_mean = initial_mean.extract(H.domain)

        if n_task != 1:
            kwargs["comm"] = comm
        if iteration < learn_gradients_until:
            kwargs["constants"] = only_learn_gradients
        elif iteration < keep_fixed_until:
            if fix_gradients == 1:
                fixed_keys.append(shift_keys_one)
                fixed_keys.append(shift_keys_two)
            kwargs["constants"] = fixed_keys

        else:
            if fix_gradients == 1:
                fixed_keys = shift_keys_one
                fixed_keys.append(shift_keys_two)
                kwargs["constants"] = fixed_keys

    kwargs["hamiltonian"] = H
    kwargs["mean"] = initial_mean
    kwargs["n_samples"] = N_samples
    kwargs["mirror_samples"] = True
    kwargs["nanisinf"] = True
    
    if iteration > 0:
        KL = ift.MetricGaussianKL(**kwargs)

        KL, convergence = minimizer(KL)
        mean = KL.position
        mean = ift.MultiField.union([initial_mean, mean])
        samples = list(KL.samples)
    else:
        H = ift.EnergyAdapter(kwargs['mean'], H, want_metric=True)
        H, conv = minimizer(H)
        mean = H.position
        samples = [ift.full(mean.domain, 0)]
        samples = 2*samples
    if master:
        name_mean = pos_fl_name.format(iteration=iteration)
        name_samples = samp_fl_name.format(iteration=iteration)
        name_mean = os.path.normpath(name_mean)
        name_samples = os.path.normpath(name_samples)
        pickle.dump(mean, open(name_mean, "wb"))
        pickle.dump(samples, open(name_samples, "wb"))
        generate_plots(config, iteration)


if __name__ == '__main__':
    if len(sys.argv) == 3:
        config = sys.argv[1]
        iteration = int(sys.argv[2])
    else:
        config = "config.properties"
        iteration = 0

    main(config, iteration)
