# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import nifty7 as ift
import numpy as np
import matplotlib.pyplot as plt
import pickle
import configparser
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
import sys
import os
from astropy.io import fits
import ast

import src.high_contrast_imaging as hci
from src.getData import get_real_data


def normpath(path):
    return os.path.normpath(path)


def double_plot(
    data1,
    data2,
    colorbar_label=None,
    plot1_label=None,
    plot2_label=None,
    path=None,
    **kwarg
):
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(11, 5), constrained_layout=True)

    im0 = axs[0].imshow(data1, origin="lower", **kwarg)
    axs[0].set_title(plot1_label)
    axs[0].get_xaxis().set_visible(False)
    axs[0].get_yaxis().set_visible(False)
    im1 = axs[1].imshow(data2, origin="lower", **kwarg)
    fig.colorbar(im0, label=colorbar_label)
    axs[1].set_title(plot2_label)
    axs[1].get_xaxis().set_visible(False)
    axs[1].get_yaxis().set_visible(False)
    fig.colorbar(im1, label=colorbar_label)
    if path is None:
        plt.show()
    else:
        plt.savefig(path, dpi=300)
        plt.close()


def post_mean_of_op(operator, position, samples=None):
    if samples is None:
        return operator.force(position)
    sc = ift.StatCalculator()
    for s in samples:
        sc.add(operator.force(s + position))
    return sc.mean


def post_var_of_op(operator, position, samples=None):
    if samples is None:
        raise NameError("No variance for map")
    sc = ift.StatCalculator()
    for s in samples:
        sc.add(operator.force(s + position))
    return sc.var


def rct_data_frames(
    model, position, samples, data, num_frames, frame_dist=1, path=None, to_fits=False, vmin=None, vmax=None, **kwarg
):
    rct = post_mean_of_op(model.image, position, samples)
    if to_fits:
        name = path + ".fits"
        if os.path.exists(name):
            os.remove(name)
        hdu = fits.PrimaryHDU(rct.val)
        hdul = fits.HDUList([hdu])
        hdul.writeto(normpath(name))
    for i in range(num_frames):
        double_plot(
            data.val[i * frame_dist, :],
            rct.val[i * frame_dist, :],
            colorbar_label="Counts",
            plot1_label="data",
            plot2_label="reconstruction",
            path=normpath(path + str(i) + ".jpg"),
            norm=LogNorm(vmin=vmin, vmax=vmax),
            cmap="hot",
            **kwarg
        )


def phase_screen_frames(
    model, position, samples, path_length, num_frames, frame_dist=1, path=None, to_fits=False, **kwarg
):
    apt = model.point_spread_function.aperture.val_rw()
    apt[apt < 0.5] = np.nan
    if path_length == "double":
        phase1 = (
            apt
            * post_mean_of_op(
                model.point_spread_function.pl.phase_screen_one, position, samples
            ).val
        )
        try:
            phase2 = (
                apt
                * post_mean_of_op(
                    model.point_spread_function.pl.phase_screen_two, position, samples
                ).val
            )
        except:
            phase2 = np.full_like(phase1, np.nan)
        if to_fits:
            try:
                hdu = fits.PrimaryHDU(phase1)
                hdul = fits.HDUList([hdu])
                hdul.writeto(normpath(path + ".fits"))
            except:
                pass
        for i in range(num_frames):
            double_plot(
                phase1[i * frame_dist, :],
                phase2[i * frame_dist],
                colorbar_label="rad",
                plot1_label="screen 1",
                plot2_label="screen 2",
                path=normpath(path + str(i)),
                **kwarg
            )
    elif not "phase2_pos_cutoff" in position.keys():
        phase = (
            apt
            * post_mean_of_op(
                model.point_spread_function.phase_screen, position, samples
            ).val
        )
        if to_fits:
            try:
                hdu = fits.PrimaryHDU(phase)
                hdul = fits.HDUList([hdu])
                hdul.writeto(normpath(path + ".fits"))
            except:
                pass

        for i in range(num_frames):
            plt.figure(figsize=(5.76, 4.32))
            plt.imshow(phase[i * frame_dist, :], origin='lower', **kwarg)
            plt.colorbar(label="rad")
            ax = plt.gca()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            plt.tight_layout(pad=0.1, h_pad=0.1, w_pad=0.1)
            if path is None:
                plt.show()
            else:
                plt.savefig(normpath(path + str(i) + ".jpg"), dpi=300)
                plt.close()


def objectIntensityPlot(
    model, position, samples, path=None, to_fits=False, **kwarg
):
    objectIntensity = post_mean_of_op(
        model.object.FFT.inverse @ model.object.object_intensity, position, samples
    ).real.val
    objectIntensity = objectIntensity[25:125, 25:125]
    objectIntensity = objectIntensity / np.max(objectIntensity)
    objectIntensity = np.where(objectIntensity < 1e-10, 1e-10, objectIntensity)
    if to_fits:
        try:
            hdu = fits.PrimaryHDU(objectIntensity)
            hdul = fits.HDUList([hdu])
            hdul.writeto(path + "_objectIntensity.fits")
        except:
            pass
    plt.figure(figsize=(5.76, 4.32))
    im = plt.imshow(objectIntensity, cmap="hot", origin="lower", **kwarg)

    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.colorbar(im, label="relative brightness")
    plt.title('object reconstruction')
    plt.tight_layout()
    if path == None:
        plt.show()
    else:
        plt.savefig(path + "objectIntensity.jpg", dpi=300)
        plt.close()


def generate_plots(config_file, iteration):
    configpars = configparser.RawConfigParser()
    configpars.read(['src/defaults.properties', config_file])
    model = hci.HighContrasImaging(configpars, 0.00)
    num_frames = model.image.target.shape[0]

    data_conf = dict(configpars.items("data_args"))
    data_path = data_conf["data_path"]
    out_path = data_conf["output_path"]

    pixel_conf = dict(configpars.items("pixel_args"))
    cut_rows = int(pixel_conf["cut_rows"])

    model_conf = dict(configpars.items("model_args"))
    path_length = model_conf["path_length"]
    psf_two_weights = ast.literal_eval(model_conf["psf_two_weights"])
    data = get_real_data(data_path, model)
    data = np.where(data > 0, data, 0.1) # for log plotting
    if cut_rows:
        data = ift.Field.from_raw(model.cut.domain, data)
        data = model.cut(data)
    else:
        data = ift.Field.from_raw(model.image.target, data)

    samp_fl_name = out_path + "/KL_samp_it_{iteration}.p"
    samp_fl_name = samp_fl_name.format(iteration=iteration)
    pos_fl_name = out_path + "/KL_pos_it_{iteration}.p"
    pos_fl_name = pos_fl_name.format(iteration=iteration)

    position = pickle.load(open(normpath(pos_fl_name), "rb"))
    samples = pickle.load(open(normpath(samp_fl_name), "rb"))

    try:
        os.mkdir(normpath(out_path))
    except OSError:
        pass
    out_name = out_path+"/iteration_"+str(iteration)
    rct_data_frames(
        model, position, samples, data, num_frames, 1, out_name + "_rct_data", vmin=1, vmax=np.nanmax(data.val), to_fits=True
    )
    phase_screen_frames(model, position, samples, path_length, num_frames, 1, out_name+'rct_phase', to_fits=False)
    objectIntensityPlot(
        model, position, samples, out_name, norm=LogNorm(vmin=5e-4), to_fits=True
    )

