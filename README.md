HCBI - Information Field Theory for High Contrast Imaging
==========================================

**HCBI** ("**H**igh **C**ontrast **B**ayesian **I**maging") is a post-processing algorithm for high cadence short exposure frames. From the frames, it infers a Bayesian estimate of the imaged object alongside the evolving PSF. The PSF is modeled by a time-dependent phase screen shaping the instantaneous wavefront. The model for the astronomical object is tailored towards high contrast imaging, containing a star in the center of the field of view surrounded by companions with unknown positions and contrasts.

Installation
------------

- Install [nifty7](https://gitlab.mpcdf.mpg.de/ift/nifty/-/tree/NIFTy_7), [ducc](https://gitlab.mpcdf.mpg.de/mtr/ducc/-/tree/ducc0), astropy, matplotlib and scipy

Usage
-----
- Adapt the config.properties file. Set path to the data fits file and other configuration options
- To run a demo with synthetic data leave "synthetic_data.fits" as a data path
- To run a reconstruction, execute the run_reconstruction.py python script. The script takes 2 input arguments
    1. The config file
    2. The number of MGVI iterations to perform

    For example, to run a reconstruction with 10 MGVI iterations, execute:

      python run_reconstruction.py config.properties 10

    Note: On a computer cluster, you will have to adapt the run_reconstruction for the workload manager software 

Demo
----
For demonstration, reconstructions from synthetic data are possible. To start a reconstruction from synthetic data, ensure that in the config file the data path is set to: "data_path = synthetic.fits". Then create and reconstruct the synthetic with:

	python run_reconstruction.py config.properties 10

This will create a synthetic data set of 5 frames imaging a star with one companion. The image of the astronomical object is affected by an atmospheric phase screen, the telescope optics, the Poisson count statistics, and Gaussian readout noise. The ground truth of the astronomical object and an exemplary data frame are depicted below:

<img src="ground_truth_synthetic.png" width="425"/> <img src="data_frame.png" width="425"/>

For each iteration of the MGVI inference, the code will generate plots. All the output can be found in the demos folder. After 10 iterations, the reconstructed object looks as follows:

<img src="./demo/iteration_9objectIntensity.jpg" width="425"/>

Also, the reconstructed phase screen, as well as the corresponding image in data space, are plotted:

<img src="./demo/iteration_9_rct_data4.jpg" width="850"/> 

<img src="./demo/iteration_9rct_phase4.png" width="850"/>

Licensing terms
---------------

The HCBI package is licensed under the terms of the
[GPLv3](https://www.gnu.org/licenses/gpl.html) and is distributed
*without any warranty*.
