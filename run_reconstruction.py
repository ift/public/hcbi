# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2022 Max-Planck-Society

import sys
from rct import main

if len(sys.argv) == 3:
    config = sys.argv[1]
    n_iter = int(sys.argv[2])
else:
    raise RuntimeError("two arguments expected: 1. config file, 2. number of iterations")


for ii in range(n_iter):
    main(config, ii)

